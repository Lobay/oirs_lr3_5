from LR3_prepare import init
from LR3_train import training, predict

if __name__ == "__main__":
    init()
    x_train, y_train = training()
    predict(x_train, y_train)