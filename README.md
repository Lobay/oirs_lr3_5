# Вариант 6: Slowpoke, Slowbro, Muk, Grimer, Lickitung, Chansey

Файл LR3_features.py используется для глобального извлечения фичей. Скрипт извлекает фичи: форму с помощью функции fd_hu_moments(), текстуру функцией fd_haralick() и цвет функцией fd_histogram()); и объединяет их в один глобальный объект (функция get_feature()).

Файл LR3_prepare.py берет по одному изображению покемона из папки data/train и сохраняет его вместе с меткой в формате HDF5 в папку output (функция init()).

В файле LR3_train.py используются функции training() и predict(). training() разделяет набор данных на обучающую и тестовую выборки, затем обучает каждую из наших моделей (Logistic Regression, k Nearest Neighbor, Decision Tree, Random Forest, GaussianNB) и проверяет результаты. 

Вывод программы:

      LR: 0.19666666666666666
                    precision    recall  f1-score   support

                 0       0.11      0.13      0.12        15
                 1       0.25      0.12      0.17        16
                 2       0.10      0.12      0.11         8
                 3       0.29      0.42      0.34        12
                 4       0.27      0.18      0.21        17
                 5       0.09      0.14      0.11         7

          accuracy                           0.19        75
         macro avg       0.19      0.19      0.18        75
      weighted avg       0.20      0.19      0.19        75

      KNN: 0.21999999999999997
                    precision    recall  f1-score   support

                 0       0.83      0.33      0.48        15
                 1       0.31      0.25      0.28        16
                 2       0.29      0.50      0.36         8
                 3       0.33      0.33      0.33        12
                 4       0.45      0.29      0.36        17
                 5       0.16      0.43      0.23         7

          accuracy                           0.33        75
         macro avg       0.40      0.36      0.34        75
      weighted avg       0.43      0.33      0.35        75

      CART: 0.34666666666666673
                    precision    recall  f1-score   support

                 0       0.89      0.53      0.67        15
                 1       0.67      0.50      0.57        16
                 2       0.31      0.62      0.42         8
                 3       0.57      0.67      0.62        12
                 4       0.55      0.35      0.43        17
                 5       0.38      0.71      0.50         7

          accuracy                           0.53        75
         macro avg       0.56      0.57      0.53        75
      weighted avg       0.60      0.53      0.54        75

      RF: 0.55
                    precision    recall  f1-score   support

                 0       0.43      0.60      0.50        15
                 1       0.44      0.25      0.32        16
                 2       0.00      0.00      0.00         8
                 3       0.53      0.67      0.59        12
                 4       0.29      0.12      0.17        17
                 5       0.26      0.71      0.38         7

          accuracy                           0.37        75
         macro avg       0.33      0.39      0.33        75
      weighted avg       0.36      0.37      0.34        75

      NB: 0.33333333333333337
                    precision    recall  f1-score   support

                 0       0.33      0.47      0.39        15
                 1       0.00      0.00      0.00        16
                 2       0.07      0.25      0.11         8
                 3       0.12      0.08      0.10        12
                 4       0.00      0.00      0.00        17
                 5       0.12      0.29      0.17         7

          accuracy                           0.16        75
         macro avg       0.11      0.18      0.13        75
      weighted avg       0.11      0.16      0.12        75


**Визуализация с помощью "ящика с усами"**

![Image alt](https://gitlab.com/Lobay/oirs_lr3_5/raw/master/data/result/box.png)



**Результат обучения**

Тестируем несколько картинок (из папки data/test) и определяем их принадлежность определенному классу с помощью классификатора random forest, так как он дал максимальную точность (~55%). Результат сохраняем в папку data/result. 

Пример результата работы:

![Image alt](https://gitlab.com/Lobay/oirs_lr3_5/raw/master/data/test/2c2451222cc14aa98cf1fb33538a69de.jpg)
![Image alt](https://gitlab.com/Lobay/oirs_lr3_5/raw/master/data/result/1.png)


**Снижение размерности:**

В файле LR3_5_pca_tsne.py строятся PCA и t-SNE распределения в двумерном пространстве и на основании полученных результатов строятся графики.

**Построение PCA и визуализация графика:**

![Image alt](https://gitlab.com/Lobay/oirs_lr3_5/raw/master/graph/PCA.png)

**Построение t-SNE и визуализация графика:**

![Image alt](https://gitlab.com/Lobay/oirs_lr3_5/raw/master/graph/t-sne.png)

