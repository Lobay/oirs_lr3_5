import h5py
import numpy as np
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
import pandas as pd
import matplotlib.pyplot as plt


def plot_2d_points(points, labels, plot_title):
    pokem = pd.DataFrame(
        list(zip(points[:, 0], points[:, 1], labels)),
        columns=['x', 'y', 'cluster']
    )
    for cluster in np.unique(pokem.cluster):
        subset = pokem[pokem.cluster == cluster]
        plt.scatter(subset.x, subset.y, label=cluster)
    plt.legend()
    plt.title(plot_title)
    plt.show()

def main():
    # импортируем вектор признаков и обученные метки
    h5f_data = h5py.File('output/data.h5', 'r')
    h5f_label = h5py.File('output/labels.h5', 'r')
    global_features_string = h5f_data['dataset_1']
    global_labels_string = h5f_label['dataset_1']
    global_features = np.array(global_features_string)
    global_labels = np.array(global_labels_string)
    h5f_data.close()
    h5f_label.close()

    print(f"[ST] features shape: {global_features.shape}")
    print(f"[ST] labels shape: {global_labels.shape}")
    print("[ST] training started...")

    # уменьшим размерность с помощью PCA и TSNE
    pca = PCA(n_components=2)
    tsne = TSNE(n_components=2)
    pca_data = pca.fit_transform(global_features)
    tsne_data = tsne.fit_transform(global_features)

    plot_2d_points(pca_data, global_labels, 'PCA of source data')
    plot_2d_points(tsne_data, global_labels, 'TSNE of source data')


if __name__ == '__main__':
    main()
