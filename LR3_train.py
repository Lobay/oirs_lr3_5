import warnings
import h5py
import matplotlib.pyplot as plt
import glob
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.metrics import confusion_matrix, accuracy_score, classification_report
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from LR3_features import *

warnings.filterwarnings('ignore')

def training():     # функция обучения моделей
    if not os.path.exists("data/test"):
        os.makedirs("data/test")
    # создание моделей обучения
    models = [('LR', LogisticRegression(random_state=9)), ('KNN', KNeighborsClassifier()),
              ('CART', DecisionTreeClassifier(random_state=9)),
              ('RF', RandomForestClassifier(n_estimators=200, random_state=9)), ('NB', GaussianNB())]
    results = []
    names = []
    # импорт векторов признаков и обученных меток
    h5f_data = h5py.File('output/data.h5', 'r')
    h5f_label = h5py.File('output/labels.h5', 'r')
    global_features_string = h5f_data['dataset_1']
    global_labels_string = h5f_label['dataset_1']
    global_features = np.array(global_features_string)
    global_labels = np.array(global_labels_string)
    h5f_data.close()
    h5f_label.close()
    print(f"[ST] features shape: {global_features.shape}")
    print(f"[ST] labels shape: {global_labels.shape}")
    print("[ST] training started...")

    # разделение данных для обучения и тестирования
    (x_train, x_test, y_train, y_test) = train_test_split(np.array(global_features), np.array(global_labels),
                                                          test_size=0.2, random_state=9)
    print("[ST] separation train and test data...")
    print(f"Train data  : {x_train.shape}")
    print(f"Test data   : {x_test.shape}")
    print(f"Train labels: {y_train.shape}")
    print(f"Test labels : {y_test.shape}")

    # обучение моделей
    for name, model in models:
        cv_results = cross_val_score(model, x_train, y_train, cv=10, scoring="accuracy", n_jobs=-1)
        model.fit(x_train, y_train)
        print(classification_report(y_test, model.predict(x_test)))
        results.append(cv_results)
        names.append(name)
        print(f'{name}: {cv_results.mean()}')

    # создаем и сохраняем сравнительную диаграмму "ящик с усами"
    fig = plt.figure()
    fig.suptitle('Machine Learning algorithm comparison')
    ax = fig.add_subplot(111)
    plt.boxplot(results)
    ax.set_xticklabels(names)
    plt.savefig('data/result/box.png')
    return x_train, y_train


def predict(train_data, train_labels):      # функция тестирования лучшей модели - random forest
    clf = RandomForestClassifier(n_estimators=200, random_state=9, n_jobs=-1)
    clf.fit(train_data, train_labels)
    for ind, file in enumerate(glob.glob("data/test" + "/*.*")):
        image = cv2.imread(file)
        image = cv2.resize(image, tuple((500, 500)))
        global_feature = get_feature(image)
        prediction = clf.predict(global_feature.reshape(1, -1))[0]
        # определение класса для картинки и ее сохранение
        fig = plt.figure()
        fig.suptitle(os.listdir("data/train")[prediction])
        plt.axis('off')
        plt.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
        plt.savefig(f'data/result/{ind}.png')