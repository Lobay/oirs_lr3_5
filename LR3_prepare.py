from sklearn.preprocessing import LabelEncoder
from LR3_features import *
import cv2
import os
import h5py


def init():
    print(os.listdir("data/train"))
    global_features = []
    labels = []
    for training_name in os.listdir("data/train"):
        current_dir = os.path.join("data/train", training_name)     # путь к папке
        images = [f for f in os.listdir(current_dir) if
                  os.path.isfile(os.path.join(current_dir, f)) and f.endswith(".jpg") or f.endswith(".png")]
        for file in images:
            file_path = os.path.join(current_dir, file)     # путь к картинке
            image = cv2.imread(file_path)
            image = cv2.resize(image, tuple((500, 500)))
            global_feature = get_feature(image)
            labels.append(training_name)         # обновляем список меток
            global_features.append(global_feature)      # обновляем вектора признаков
        print(f"[ST] folder: {training_name}")

    # кодировка меток и создание HDF5 файлов
    le = LabelEncoder()
    target = le.fit_transform(labels)
    h5f_data = h5py.File('output/data.h5', 'w')
    h5f_data.create_dataset('dataset_1', data=np.array(global_features))
    h5f_label = h5py.File('output/labels.h5', 'w')
    h5f_label.create_dataset('dataset_1', data=np.array(target))
    h5f_data.close()
    h5f_label.close()
    print("[ST] end")